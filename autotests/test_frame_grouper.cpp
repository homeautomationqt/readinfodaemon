/*
 * Copyright 2012 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_frame_grouper.h"

#include "frame_grouper.h"

#include <QtCore/QBuffer>
#include <QtCore/QMetaType>
#include <QtCore/QTextStream>

#include <QtTest/QSignalSpy>
#include <QtTest/QTest>

Q_DECLARE_METATYPE(QList<QByteArray>);

TestFrameGrouper::TestFrameGrouper()
{
    qRegisterMetaType<QList<QByteArray> >();
}

void TestFrameGrouper::nominalTest()
{
    FrameGrouper theFrameGrouper;

    QSignalSpy theFrameGrouperSpy(&theFrameGrouper, SIGNAL(newFrame(QList<QByteArray>)));

    QBuffer theBuffer;

    theBuffer.open(QBuffer::ReadWrite);

    QSignalSpy theBufferSpy(&theBuffer, SIGNAL(bytesWritten(qint64)));

    //theFrameGrouper.setSerialPort(&theBuffer);

    const char testData[] = "ADCO 049922076568 Q\nOPTARIF HC.. <\nISOUSC 45 ?\nHCHC 061570362 $\nHCHP 062965203 4\nPTEC HC.. S\nIINST 010 X\nIMAX 049 L\nPAPP 02200 %\nHHPHC A ,\nMOTDETAT 000000 B";
    theBuffer.write(testData, sizeof(testData));
    theBuffer.buffer() += QByteArray::fromRawData(testData, sizeof(testData));
    theBuffer.buffer() += QByteArray::fromRawData(testData, sizeof(testData));
    theBuffer.buffer() += QByteArray::fromRawData(testData, sizeof(testData));

    QCOMPARE(theBufferSpy.count(), 1);
    QCOMPARE(theFrameGrouperSpy.count(), 1);
}

