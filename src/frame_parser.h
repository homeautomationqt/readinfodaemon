/*
 * Copyright 2012-2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined FRAME_PARSER_H
#define FRAME_PARSER_H

#include <QtCore/QtGlobal>
#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtCore/QByteArray>

class FrameParserPrivate;

class FrameParser : public QObject
{
    Q_OBJECT

    Q_CLASSINFO("D-Bus Interface", "org.mgallien.RemoteInformation")

    Q_PROPERTY(qint64 counterAddress READ counterAddress)

    Q_PROPERTY(int subscribdedMaximumCurrent READ subscribdedMaximumCurrent)

    Q_PROPERTY(bool isFullHours READ isFullHours NOTIFY hourTypeChanged)

    Q_PROPERTY(qint32 fullHoursCounter READ fullHoursCounter NOTIFY fullHoursCounterChanged)

    Q_PROPERTY(qint32 emptyHoursCounter READ emptyHoursCounter NOTIFY emptyHoursCounterChanged)

    Q_PROPERTY(qint32 currentElectricCurrent READ currentElectricCurrent NOTIFY currentElectricCurrentChanged)

    Q_PROPERTY(qint32 maximumElectricCurrent READ maximumElectricCurrent NOTIFY maximumElectricCurrentChanged)

    Q_PROPERTY(qint32 currentPowerUsage READ currentPowerUsage NOTIFY currentPowerUsageChanged)

    Q_PROPERTY(bool overElectricPowerUsage READ overElectricPowerUsage NOTIFY overElectricPowerUsageChanged)

public:

    FrameParser(QObject *parent = 0);

Q_SIGNALS:

    void hourTypeChanged(const QString &objectName, const char *propertyName, const QVariant &value);

    void fullHoursCounterChanged(const QString &objectName, const char *propertyName, const QVariant &value);

    void emptyHoursCounterChanged(const QString &objectName, const char *propertyName, const QVariant &value);

    void currentElectricCurrentChanged(const QString &objectName, const char *propertyName, const QVariant &value);

    void maximumElectricCurrentChanged(const QString &objectName, const char *propertyName, const QVariant &value);

    void currentPowerUsageChanged(const QString &objectName, const char *propertyName, const QVariant &value);

    void overElectricPowerUsageChanged(const QString &objectName, const char *propertyName, const QVariant &value);

public Q_SLOTS:

    void newFrame(const QList<QByteArray> &frame);

    qint64 counterAddress() const;

    int subscribdedMaximumCurrent() const;

    bool isFullHours() const;

    qint32 fullHoursCounter() const;

    qint32 emptyHoursCounter() const;

    qint32 currentElectricCurrent() const;

    qint32 maximumElectricCurrent() const;

    qint32 currentPowerUsage() const;

    char fullHoursScheduleType() const;

    bool overElectricPowerUsage() const;

private Q_SLOTS:

    void emitHourTypeChanged(const QString &objectName, const char *propertyName, const QVariant &value);

    void emitFullHoursCounterChanged(const QString &objectName, const char *propertyName, const QVariant &value);

    void emitEmptyHoursCounterChanged(const QString &objectName, const char *propertyName, const QVariant &value);

    void emitCurrentElectricCurrentChanged(const QString &objectName, const char *propertyName, const QVariant &value);

    void emitMaximumElectricCurrentChanged(const QString &objectName, const char *propertyName, const QVariant &value);

    void emitCurrentPowerUsageChanged(const QString &objectName, const char *propertyName, const QVariant &value);

    void emitOverElectricPowerUsageChanged(const QString &objectName, const char *propertyName, const QVariant &value);

private:

    FrameParserPrivate *d;
};

#endif
