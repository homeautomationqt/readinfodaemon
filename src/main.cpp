/*
 * Copyright 2012-2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "frame_grouper.h"
#include "frame_parser.h"

#include <automatiq/AutomatiqHost>
#include <automatiq/DataManager>
#include <automatiq/ServerConfigManager>
#include <automatiq/ProtocolManager>
#include <automatiq/RuntimeManager>

#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

#include <QtNetwork/QHostInfo>

#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>
#include <QtCore/QSettings>
#include <QtCore/QThread>
#include <QtCore/QTimer>

#include <systemd/sd-daemon.h>

int main(int argc, char *argv[])
{
    QCoreApplication theApp(argc, argv);

    QSettings mySettings(QStringLiteral("org.mgallien"), QStringLiteral("readinfodaemon"));

    QSerialPort thePort(mySettings.value(QStringLiteral("serialPort"), QStringLiteral("/dev/ttyS1")).toString());

    thePort.open(QIODevice::ReadOnly);
    thePort.setBaudRate(QSerialPort::Baud1200);
    thePort.setDataBits(QSerialPort::Data7);
    thePort.setParity(QSerialPort::EvenParity);
    thePort.setStopBits(QSerialPort::OneStop);
    thePort.setFlowControl(QSerialPort::NoFlowControl);

    FrameGrouper theGrouper;

    theGrouper.setSerialPort(&thePort);

    FrameParser theParser;
    theParser.setObjectName(QStringLiteral("electricInformations"));

    theApp.connect(&theGrouper, SIGNAL(newFrame(QList<QByteArray>)), &theParser, SLOT(newFrame(QList<QByteArray>)));

    AutomatiqHost *newHost = new AutomatiqHost(&theApp);
    newHost->setHostPeerName(QHostInfo::localHostName());
    newHost->initialize(QHostAddress::Any, 4243);

    RuntimeManager *myHostRunTime = new RuntimeManager(newHost, &theApp);
    myHostRunTime->setHostPeerName(QHostInfo::localHostName());
    myHostRunTime->initialize();

    myHostRunTime->registerVariable(&theParser, QStringLiteral("counterAddress"));
    myHostRunTime->registerVariable(&theParser, QStringLiteral("subscribdedMaximumCurrent"));
    myHostRunTime->registerVariable(&theParser, QStringLiteral("isFullHours"));
    myHostRunTime->registerVariable(&theParser, QStringLiteral("fullHoursCounter"));
    myHostRunTime->registerVariable(&theParser, QStringLiteral("emptyHoursCounter"));
    myHostRunTime->registerVariable(&theParser, QStringLiteral("currentElectricCurrent"));
    myHostRunTime->registerVariable(&theParser, QStringLiteral("currentPowerUsage"));
    myHostRunTime->registerVariable(&theParser, QStringLiteral("overElectricPowerUsage"));

    uint64_t usecWatchdog;

    int systemdWatchdogStatus = sd_watchdog_enabled(0, &usecWatchdog);

    if (systemdWatchdogStatus > 0) {
        qDebug() << "activate watchdog";

        QTimer *watchdogTimer = new QTimer(&theApp);
        watchdogTimer->start(usecWatchdog / 1000 / 2);

        QObject::connect(watchdogTimer, &QTimer::timeout, [=] () {
            sd_notify(0, "WATCHDOG=1");
        });
    }

    return theApp.exec();
}

