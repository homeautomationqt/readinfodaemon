/*
 * Copyright 2012-2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "frame_grouper.h"

#include <QtCore/QIODevice>
#include <QtCore/QBuffer>
#include <QtCore/QDebug>
#include <QtCore/QByteArrayMatcher>

class FrameGrouperPrivate
{
public:
    FrameGrouperPrivate() : serialPort(0), currentFrameGroup(),
                            adcoLine("ADCO"), motdetatLine("MOTDETAT"),
                            validGroup(true)
    {
    }

    QIODevice *serialPort;
    QList<QByteArray> currentFrameGroup;
    bool newFrame;
    QByteArrayMatcher adcoLine;
    QByteArrayMatcher motdetatLine;
    bool validGroup;
};

FrameGrouper::FrameGrouper(QObject *parent) : QObject(parent), d(new FrameGrouperPrivate)
{
}

void FrameGrouper::setSerialPort(QIODevice *port)
{
    if (d->serialPort) {
        disconnect(d->serialPort);
    }
    d->serialPort = port;
    connect(d->serialPort, SIGNAL(readyRead()), this, SLOT(serialPortReadyRead()));
}

const QIODevice* FrameGrouper::serialPort() const
{
    return d->serialPort;
}

bool FrameGrouper::checkLineValidity(const QByteArray &frameLine) const
{
    char lrc = 0;
    for (int i = 0; i < frameLine.size() - 2; ++i) {
        lrc += frameLine[i];
    }
    lrc = lrc & 0x3F;
    lrc += 0x20;
    const bool res = (lrc == frameLine[frameLine.size() - 1]);
    return res;
}

void FrameGrouper::serialPortReadyRead()
{
    if (d->serialPort->canReadLine()) {
        QByteArray temp = d->serialPort->readLine();

        if (d->adcoLine.indexIn(temp) == 0) {
            temp.chop(2);
            d->currentFrameGroup.clear();
            d->validGroup = checkLineValidity(temp);
            if (d->validGroup) {
                d->currentFrameGroup.push_back(temp);
            }
        } else {
            if (d->motdetatLine.indexIn(temp) == 0) {
                temp.chop(4);
                d->validGroup = d->validGroup && checkLineValidity(temp);
                if (d->validGroup) {
                    d->currentFrameGroup.push_back(temp);
                    Q_EMIT newFrame(d->currentFrameGroup);
                }
            } else {
                temp.chop(2);
                d->validGroup = d->validGroup && checkLineValidity(temp);
                if (d->validGroup) {
                    d->currentFrameGroup.push_back(temp);
                }
            }
        }
    }
}

#include "frame_grouper.moc"
