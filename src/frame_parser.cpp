/*
 * Copyright 2012-2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "frame_parser.h"
//#include "remoteinformationadaptor.h"

#include <QtCore/QtGlobal>
#include <QtCore/QObject>
#include <QtCore/QByteArrayMatcher>
#include <QtCore/QTimer>
#include <QtCore/QDebug>

class FrameParserPrivate
{
public:

    FrameParserPrivate() : counterAddress(0), subscribdedMaximumCurrent(0), isFullHours(true), fullHoursCounter(0),
        emptyHoursCounter(0), currentElectricCurrent(0), maximumElectricCurrent(0), currentPowerUsage(0),
        fullHoursScheduleType(' '), overElectricPowerUsage(false), adcoLine("ADCO"), optarifLine("OPTARIF"),
        isouscLine("ISOUSC"), hchcLine("HCHC"), hchpLine("HCHP"), ptecLine("PTEC"), iinstLine("IINST"),
        adpsLine("ADPS"), imaxLine("IMAX"), pappLine("PAPP"), hhphcLine("HHPHC"), motdetatLine("MOTDETAT"),
        patternFullHours("HP..")
    {
    }

    qulonglong counterAddress;

    int subscribdedMaximumCurrent;

    bool isFullHours;

    qint32 fullHoursCounter;

    qint32 emptyHoursCounter;

    qint32 currentElectricCurrent;

    qint32 maximumElectricCurrent;

    qint32 currentPowerUsage;

    char fullHoursScheduleType;

    bool overElectricPowerUsage;

    QByteArrayMatcher adcoLine;
    QByteArrayMatcher optarifLine;
    QByteArrayMatcher isouscLine;
    QByteArrayMatcher hchcLine;
    QByteArrayMatcher hchpLine;
    QByteArrayMatcher ptecLine;
    QByteArrayMatcher iinstLine;
    QByteArrayMatcher adpsLine;
    QByteArrayMatcher imaxLine;
    QByteArrayMatcher pappLine;
    QByteArrayMatcher hhphcLine;
    QByteArrayMatcher motdetatLine;

    QByteArray patternFullHours;
};

FrameParser::FrameParser(QObject *parent) : QObject(parent), d(new FrameParserPrivate)
{
    //new RemoteInformationAdaptor(this);
    //QDBusConnection dbus = QDBusConnection::systemBus();
    //dbus.registerObject(QString::fromUtf8("/RemoteInformation"), this);
    //dbus.registerService(QString::fromUtf8("org.mgallien.RemoteInformation"));
}

qint64 FrameParser::counterAddress() const
{
    return d->counterAddress;
}

int FrameParser::subscribdedMaximumCurrent() const
{
    return d->subscribdedMaximumCurrent;
}

bool FrameParser::isFullHours() const
{
    return d->isFullHours;
}

qint32 FrameParser::fullHoursCounter() const
{
    return d->fullHoursCounter;
}

qint32 FrameParser::emptyHoursCounter() const
{
    return d->emptyHoursCounter;
}

qint32 FrameParser::currentElectricCurrent() const
{
    return d->currentElectricCurrent;
}

qint32 FrameParser::maximumElectricCurrent() const
{
    return d->maximumElectricCurrent;
}

qint32 FrameParser::currentPowerUsage() const
{
    return d->currentPowerUsage;
}

char FrameParser::fullHoursScheduleType() const
{
    return d->fullHoursScheduleType;
}

bool FrameParser::overElectricPowerUsage() const
{
    return d->overElectricPowerUsage;
}

void FrameParser::newFrame(const QList<QByteArray> &frame)
{
    bool overPowerDetected = false;

    for (QList<QByteArray>::const_iterator itLine = frame.begin(); itLine != frame.end(); ++itLine) {
        if (d->adcoLine.indexIn(*itLine) == 0) {
            d->counterAddress = itLine->split(' ')[1].toULongLong();
        }

        if (d->isouscLine.indexIn(*itLine) == 0) {
            d->subscribdedMaximumCurrent = itLine->split(' ')[1].toInt();
        }

        if (d->hchpLine.indexIn(*itLine) == 0) {
            qint32 newValue = itLine->split(' ')[1].toInt();
            if (d->fullHoursCounter != newValue) {
                d->fullHoursCounter = newValue;
                Q_EMIT fullHoursCounterChanged(objectName(), "fullHoursCounter", d->fullHoursCounter);
            }
        }

        if (d->hchcLine.indexIn(*itLine) == 0) {
            qint32 newValue = itLine->split(' ')[1].toInt();
            if (d->emptyHoursCounter != newValue) {
                d->emptyHoursCounter = newValue;
                Q_EMIT emptyHoursCounterChanged(objectName(), "emptyHoursCounter", d->emptyHoursCounter);
            }
        }

        if (d->ptecLine.indexIn(*itLine) == 0) {
            bool isDetectedFullHours = itLine->split(' ')[1] == d->patternFullHours;
            if (d->isFullHours != isDetectedFullHours) {
                d->isFullHours = isDetectedFullHours;
                Q_EMIT hourTypeChanged(objectName(), "isFullHours", d->isFullHours);
            }
        }

        if (d->iinstLine.indexIn(*itLine) == 0) {
            qint32 newValue = itLine->split(' ')[1].toInt();
            if (d->currentElectricCurrent != newValue) {
                d->currentElectricCurrent = newValue;
                Q_EMIT currentElectricCurrentChanged(objectName(), "currentElectricCurrent", d->currentElectricCurrent);
            }
        }

        if (d->adpsLine.indexIn(*itLine) == 0) {
            overPowerDetected = true;
        }

        if (d->imaxLine.indexIn(*itLine) == 0) {
            qint32 newValue = itLine->split(' ')[1].toInt();
            if (d->maximumElectricCurrent != newValue) {
                d->maximumElectricCurrent = newValue;
                Q_EMIT maximumElectricCurrentChanged(objectName(), "maximumElectricCurrent", d->maximumElectricCurrent);
            }
        }

        if (d->pappLine.indexIn(*itLine) == 0) {
            qint32 newValue = itLine->split(' ')[1].toInt();
            if (d->currentPowerUsage != newValue) {
                d->currentPowerUsage = newValue;
                Q_EMIT currentPowerUsageChanged(objectName(), "currentPowerUsage", d->currentPowerUsage);
                qDebug() << "currentPowerUsage" << d->currentPowerUsage;
            }
        }

        if (d->hhphcLine.indexIn(*itLine) == 0) {
            d->fullHoursScheduleType = itLine->split(' ')[1][0];
        }
    }

    if (d->overElectricPowerUsage != overPowerDetected) {
        d->overElectricPowerUsage = overPowerDetected;
        Q_EMIT overElectricPowerUsageChanged(objectName(), "overElectricPowerUsage", d->overElectricPowerUsage);
    }
}

void FrameParser::emitHourTypeChanged(const QString &objectName, const char *propertyName, const QVariant &value)
{
    Q_EMIT hourTypeChanged(objectName, propertyName, value);
}

void FrameParser::emitFullHoursCounterChanged(const QString &objectName, const char *propertyName, const QVariant &value)
{
    Q_EMIT fullHoursCounterChanged(objectName, propertyName, value);
}

void FrameParser::emitEmptyHoursCounterChanged(const QString &objectName, const char *propertyName, const QVariant &value)
{
    Q_EMIT emptyHoursCounterChanged(objectName, propertyName, value);
}

void FrameParser::emitCurrentElectricCurrentChanged(const QString &objectName, const char *propertyName, const QVariant &value)
{
    Q_EMIT currentElectricCurrentChanged(objectName, propertyName, value);
}

void FrameParser::emitMaximumElectricCurrentChanged(const QString &objectName, const char *propertyName, const QVariant &value)
{
    Q_EMIT maximumElectricCurrentChanged(objectName, propertyName, value);
}

void FrameParser::emitCurrentPowerUsageChanged(const QString &objectName, const char *propertyName, const QVariant &value)
{
    Q_EMIT currentPowerUsageChanged(objectName, propertyName, value);
}

void FrameParser::emitOverElectricPowerUsageChanged(const QString &objectName, const char *propertyName, const QVariant &value)
{
    Q_EMIT overElectricPowerUsageChanged(objectName, propertyName, value);
}

#include "frame_parser.moc"
